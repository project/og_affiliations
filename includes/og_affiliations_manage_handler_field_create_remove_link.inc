<?php

/**
 * Renders a create/remove link.
 */
class og_affiliations_manage_handler_field_create_remove_link extends views_handler_field {
  
  /**
   * Add our status (is_active) field to the result set.
   */
  function construct() {
    parent::construct();
    $this->additional_fields['is_active'] = 'is_active';
  }
  
  /**
   * Replaces our NID placeholder in the query; simpler than combining a parameter and field.
   */
  function query() {
    if (arg(0) == 'node' && is_numeric(arg(1))) {
      $nid = arg(1);
    }
    else {
      $str = $this->query->table_queue['og_affiliations_manage']['join']->extra;
      $nid = substr($str, strrpos($str, ' ') + 1);
    }
    if ($nid && is_numeric($nid)) {
      $this->og_current_nid = $nid;
    }
    $this->add_additional_fields();
  }
  
  /**
   * Renders the link output.
   * 
   * @return mixed
   */
  function render($values) {
    if ($this->og_current_nid) {
      // none
      if (!$values->is_active) {
        $link = l(t('Click to Create/Request Affiliation'), 'og/affiliations/create/'.$this->og_current_nid.'/'.$values->nid);
      }
      // pending
      else if ($values->is_active == OG_AFFILIATIONS_STATUS_PENDING) {
        $link = t('Request is Pending Approval');
      }
      // active
      else if ($values->is_active == OG_AFFILIATIONS_STATUS_ACTIVE){
        $link = l(t('Click to Remove Affiliation'), 'og/affiliations/remove/'.$this->og_current_nid.'/'.$values->nid);
      }
    }
    return ($link ? $link : NULL);
  }
}
