<?php

/**
 * Renders an approve/deny link, but does NOT enforce pending status.
 */
class og_affiliations_in_handler_field_approve_deny_link extends views_handler_field {
  
  /**
   * Replaces our NID placeholder in the query; simpler than combining a parameter and field.
   */
  function query() {
    foreach ($this->query->where[0]['clauses'] as $id => $clause) {
      if ($clause == 'og_affiliations_in.nid = %d') {
        $nid = $this->query->where[0]['args'][$id];
        break;
      }
    }
    if ($nid && is_numeric($nid)) {
      $this->og_current_nid = $nid;
    }
  }
  
  /**
   * Renders the link output.
   * 
   * @return mixed
   */
  function render($values) {
    if ($this->og_current_nid) {
      $token = og_get_token($this->og_current_nid);
      $links = l(t('Click to Approve Request'), 'og/affiliations/approve/'.$this->og_current_nid.'/'.$values->nid.'/'.$token);
      $links .= '&nbsp;'.l(t('Click to Deny Request'), 'og/affiliations/deny/'.$this->og_current_nid.'/'.$values->nid.'/'.$token);
    }
    return ($links ? $links : NULL);
  }
}
