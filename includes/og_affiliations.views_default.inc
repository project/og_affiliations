<?php

/**
 * Implementation of hook_views_default_views().
 * 
 * @return array
 */
function og_affiliations_views_default_views() {
  $files = file_scan_directory(drupal_get_path('module', 'og_affiliations').'/includes/views', '.view');
  $views = array();
  foreach ($files as $absolute => $file) {
    require $absolute;
    if (isset($view)) {
      $views[$file->name] = $view;
    }
  }
  return $views;
}
