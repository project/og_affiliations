<?php

/**
 * NID argument handler, allows us to avoid using custom join class.
 */
class og_affiliations_manage_handler_argument_nid extends views_handler_argument_numeric {
  
  /**
   * Defines a custom join so we don't have to use a relationship or custom views_join class.
   */
  function query() {
    $args = explode(',', $this->argument);
    $node = node_load((int)$args[0]);
    
    $join = new views_join();
    $join->definition = array(
      'table' => 'og_affiliations',
      'field' => 'aid',
      'left_table' => 'node',
      'left_field' => 'nid',
    );
    $join->definition['extra'] = 'og_affiliations_manage.nid = '.$args[0];
    $join->construct();
    
    $this->alias = $this->query->add_table('og_affiliations', NULL, $join, 'og_affiliations_manage');
  }
}
