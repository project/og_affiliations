<?php

/**
 * Implementation of hook_views_handlers().
 * 
 * @return array
 */
function og_affiliations_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'og_affiliations').'/includes',
    ),
    'handlers' => array(
      // arguments
      'og_affiliations_manage_handler_argument_nid' => array(
        'parent' => 'views_handler_argument_numeric',
      ),
      // fields
      'og_affiliations_in_handler_field_approve_deny_link' => array(
        'parent' => 'views_handler_field',
      ),
      'og_affiliations_in_handler_field_remove_link' => array(
        'parent' => 'views_handler_field',
      ),
      'og_affiliations_manage_handler_field_create_remove_link' => array(
        'parent' => 'views_handler_field',
      ),
      // filters
      'og_affiliations_manage_handler_filter_is_group_admin' => array(
        'parent' => 'views_handler_filter_boolean_operator',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data().
 * 
 * @return array
 */
function og_affiliations_views_data() {
  /**
   * Affiliations in (starting with NID, get AIDs which are affiliated to NID)
   */
  $data['og_affiliations_in']['table'] = array(
    'group' => t('Organic groups: Affiliations to current group'),
    'join' => array(
      'node' => array(
        'left_field' => 'nid',
        'field' => 'aid',
        'table' => 'og_affiliations',
        'type' => 'INNER',
      ),
    ),
  );
  
  $data['og_affiliations_in']['nid'] = array(
    'title' => t('Nid'),
    'help' => t('The group node Id.'),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'nid',
      'help' => t('Used to return/display groups which are affiliated to the provided group.'),
    ),
  );
  
  $data['og_affiliations_in']['is_active'] = array(
    'title' => t('Active?'),
    'help' => t('Whether or not the affilition is active.'),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'title',
      'numeric' => TRUE,
      'help' => t('Provide the affiliation status (PENDING == 0, ACTIVE == 1).'),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'help' => t('Filter by affiliation status (PENDING == 0, ACTIVE == 1).'),
    ),
  );
  
  $data['og_affiliations_in']['created'] = array(
    'title' => t('Created'),
    'help' => t('The date on which this affiliation was created.'),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );
  
  $data['og_affiliations_in']['changed'] = array(
    'title' => t('Changed'),
    'help' => t('The date on which this affiliation was changed.'),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );
  
  $data['og_affiliations_in']['approve_deny'] = array(
    'field' => array(
      'title' => t('Approve/Deny link'),
      'help' => t('Provide a link to approve/deny affiliation.'),
      'handler' => 'og_affiliations_in_handler_field_approve_deny_link',
    ),
  );
  
  $data['og_affiliations_in']['remove_group_affiliation'] = array(
    'title' => t('Remove group affiliation link'),
    'help' => t('Provide a link to remove group affiliation.'),
    'field' => array(
      'handler' => 'og_affiliations_in_handler_field_remove_link',
    ),
  );
  
  /**
   * Affiliations out (starting with AID, get NIDs to which AID is affiliated)
   */
  $data['og_affiliations_out']['table'] = array(
    'group' => t('Organic groups: Affiliations to other groups'),
    'join' => array(
      'node' => array(
        'left_field' => 'nid',
        'field' => 'nid',
        'table' => 'og_affiliations',
        'type' => 'INNER',
      ),
    ),
  );
  
  $data['og_affiliations_out']['aid'] = array(
    'title' => t('Aid'),
    'help' => t('The affiliate group node Id.'),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'nid',
      'help' => t('Used to return/display groups to which the provided group is affiliated.'),
    ),
  );
  
  $data['og_affiliations_out']['is_active'] = array(
    'title' => t('Active?'),
    'help' => t('Whether or not the affilition is active.'),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'title',
      'numeric' => TRUE,
      'help' => t('Provide the affiliation status (PENDING == 0, ACTIVE == 1).'),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'help' => t('Filter by affiliation status (PENDING == 0, ACTIVE == 1).'),
    ),
  );
  
  $data['og_affiliations_out']['created'] = array(
    'title' => t('Created'),
    'help' => t('The date on which this affiliation was created.'),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );
  
  $data['og_affiliations_out']['changed'] = array(
    'title' => t('Changed'),
    'help' => t('The date on which this affiliation was changed.'),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );
  
  /**
   * Manage affiliations (similar to 'in', but includes potential affiliations which don't yet exist)
   */
  $data['og_affiliations_manage']['table'] = array(
    'group' => t('Organic groups: Manage affiliations from other groups the user administers'),
    'join' => array(
      'node' => array(
        'left_field' => 'nid',
        'field' => 'aid',
        'table' => 'og_affiliations',
      ),
    ),
  );
  
  $data['og_affiliations_manage']['nid'] = array(
    'title' => t('Nid'),
    'help' => t('The group node Id.'),
    'argument' => array(
      'handler' => 'og_affiliations_manage_handler_argument_nid',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'nid',
      'help' => t('Used to return/display groups which can be affiliated to the provided group.'),
    ),
  );
  
  $data['og_affiliations_manage']['create_remove_group_affiliation'] = array(
    'title' => t('Create/Remove group affiliation link'),
    'help' => t('Provide a link to create/remove group affiliation.'),
    'field' => array(
      'handler' => 'og_affiliations_manage_handler_field_create_remove_link',
    ),
  );
  
  $data['og_affiliations_manage']['is_group_admin'] = array(
    'title' => t('Current user administers other group(s)?'),
    'help' => t('Whether the current user administers another group.'),
    'filter' => array(
      'handler' => 'og_affiliations_manage_handler_filter_is_group_admin',
      'help' => t('Filter depending on whether the current user administers another group.'),
    ),
  );
  
  return $data;
}
