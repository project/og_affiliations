<?php

/**
 * Filters nodes (node.nid) based on if the current user administers the [group] node.
 */
class og_affiliations_manage_handler_filter_is_group_admin extends views_handler_filter_boolean_operator {
  
  /**
   * Set the text properly in Views UI.
   */
  function construct() {
    parent::construct();
    $this->value_value = t('Current user administers the group');
  }
  
  /**
   * Adds our vertical partition (where clause) to the Views query.
   */
  function query() {
    $groups = og_affiliations_get_groups();
    $groups = $groups ? implode(',', $groups) : 'NULL';
    $where = 'node.nid'.(empty($this->value) ? ' NOT ' : ' ').'IN ('.$groups.') ';
    $this->query->add_where('og_affiliations_is_group_admin', $where);
  }
}
