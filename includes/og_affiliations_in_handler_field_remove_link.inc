<?php

/**
 * Renders a remove link, but does NOT enforce active status.
 */
class og_affiliations_in_handler_field_remove_link extends views_handler_field {
  
  /**
   * Replaces our NID placeholder in the query; simpler than combining a parameter and field.
   */
  function query() {
    foreach ($this->query->where[0]['clauses'] as $id => $clause) {
      if ($clause == 'og_affiliations_in.nid = %d') {
        $nid = $this->query->where[0]['args'][$id];
        break;
      }
    }
    if ($nid && is_numeric($nid)) {
      $this->og_current_nid = $nid;
    }
  }
  
  /**
   * Renders the link output.
   * 
   * @return mixed
   */
  function render($values) {
    if ($this->og_current_nid) {
      $link = l(t('Click to Remove Affiliation'), 'og/affiliations/remove/'.$this->og_current_nid.'/'.$values->nid);
    }
    return ($link ? $link : NULL);
  }
}
