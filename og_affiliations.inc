<?php

// include style sheet(s)
drupal_add_css(drupal_get_path('module', 'og_affiliations').'/affiliations.css');

// define file so we can avoid using require_once()
define('__OG_AFFILIATIONS_INC__', TRUE);

// affiliation states
define('OG_AFFILIATIONS_STATUS_PENDING', 0);
define('OG_AFFILIATIONS_STATUS_ACTIVE', 1);

// SQL
define('OG_AFFILIATIONS_SQL_DELETE', "DELETE FROM {og_affiliations} WHERE nid = %d AND aid = %d");
define('OG_AFFILIATIONS_SQL_EXISTS', "SELECT COUNT(*) FROM {og_affiliations} WHERE nid = %d AND aid = %d");
define('OG_AFFILIATIONS_SQL_EXISTS_BY_STATUS', "SELECT COUNT(*) FROM {og_affiliations} WHERE nid = %d AND aid = %d AND is_active = %d");

/**
 * Callback which attempts to affiliate $affiliate to $node, invoked from the affiliation confirmation
 * form submission.  Will return message based on success or failure of requested action.
 * 
 * @param object $node
 * @param object $affiliate
 * @return array
 */
function og_affiliations_affiliate($node, $affiliate) {
  // admins can add affiliates to their own groups regardless of permissions
  if (og_is_group_admin($node) || $node->og_selective == OG_OPEN) {
    og_affiliations_save($node->nid, $affiliate->nid, array('is_active' => OG_AFFILIATIONS_STATUS_ACTIVE));
    return array(
      'type' => 'affiliated',
      'message' => t('%a is now affiliated with %g.', array('%a' => $affiliate->title, '%g' => $node->title))
    );
  }
  else if ($node->og_selective == OG_MODERATED) {
    // moderated groups must approve all affiliations (selective == 1)
    og_affiliations_save($node->nid, $affiliate->nid, array('is_active' => OG_AFFILIATIONS_STATUS_PENDING));
    return array(
      'type' => 'approval',
      'message' => t('Request to affiliate %a with %g awaits approval by an administrator.', array('%a' => $affiliate->title, '%g' => $node->title))
    );
  }
  return array(
    'type' => 'rejected',
    'message' => t('Request to affiliate %a with %g was rejected, only administrators can add affiliates to this group.', array('%a' => $affiliate->title, '%group' => $node->title))
  );
}

/**
 * Callback which approves an affiliation request.
 * 
 * @param object $node
 * @param object $affiliate
 * @param string $token
 * @return mixed
 */
function og_affiliations_approve($node, $affiliate, $token) {
  if (!og_check_token($token, $node->nid)) {
    drupal_set_message(t('Bad token. You seem to have followed an invalid link.'), 'error');
    drupal_access_denied();
    return;
  }
  
  if (og_affiliations_is_affiliated($node->nid, $affiliate->nid)) {
    drupal_set_message(t('%a is already affiliated with %g.', array('%a' => $affiliate->title, '%g' => $node->title)), 'error');
    return '';
  }
  else {
    og_affiliations_save($node->nid, $affiliate->nid, array('is_active' => 1));
    drupal_set_message(t('Affiliation request approved.'));
    
//TODO
//    module_invoke_all('og', 'approve group affiliation', $node->nid, $affiliate->nid, $message);
    drupal_goto('node/'.$node->nid);
  }
}

/**
 * Callback which produces output for the affiliations blocks.  Block content comes from Views.
 * 
 * @param object $node
 * @param string $delta
 * @return array
 */
function og_affiliations_block_affiliations($node, $delta) {
  $block = NULL;
  list($group, $direction) = explode('--', $delta);
  if (og_affiliations_is_affiliate_type($node->type, $group, $direction == 'in')) {
    $content = views_embed_view('og_affiliations_'.$direction.'_active', 'block_1', $node->nid, $group);
    if ($content) {
      $subject = ($direction == 'in' ? '%t groups currently affiliated to %g' : '%t groups to which %g is currently affiliated');
      $block = array(
        'subject' => t($subject, array('%t' => node_get_types('name', $group), '%g' => $node->title)),
        'content' => $content,
      );
    }
  }
  return $block;
}

/**
 * Callback which deletes an affiliation.
 * 
 * @param integer $nid
 * @param integer $aid
 * @param $args=array()
 */
function og_affiliations_delete($nid, $aid, $args = array()) {
  db_query(OG_AFFILIATIONS_SQL_DELETE, $nid, $aid);
  
//TODO
//  module_invoke_all('og', 'delete group affiliation', $nid, $aid, $args);
}

/**
 * Callback which denies an affiliation request.
 * 
 * @param object $node
 * @param object $affiliate
 * @param string $token
 * @return mixed
 */
function og_affiliations_deny($node, $affiliate, $token) {
  if (!og_check_token($token, $node->nid)) {
    drupal_set_message(t('Bad token. You seem to have followed an invalid link.'), 'error');
    drupal_access_denied();
    return;
  }
  og_affiliations_delete($node->nid, $affiliate->nid);
  drupal_set_message(t('Affiliation request denied.'));
  
//TODO
//  module_invoke_all('og', 'deny group affiliation', $node->nid, $affiliate->nid, $message);
  drupal_goto('node/'.$node->nid);
}

/**
 * Returns the valid group types that can be either affiliated to $type (default, $in=TRUE),
 * or that $type can be affiliated to ($in=FALSE).
 * 
 * @param string $type
 * @param boolean $in=TRUE
 * @return array
 */
function og_affiliations_get_affiliate_types($type, $in = TRUE) {
  $groups = og_affiliations_get_group_types();
  $types = array();
  foreach ($groups as $group) {
    if (og_affiliations_is_affiliate_type($type, $group, $in)) {
      $types[] = $group;
    }
  }
  return $types;
}

/**
 * Returns an array containing all the group node types.
 * 
 * @return array
 */
function og_affiliations_get_group_types() {
  $groups = node_get_types('types');
  $types = array();
  foreach ($groups as $group) {
    if (og_is_group_type($group->type)) {
      $types[] = $group->type;
    }
  }
  return $types;
}

/**
 * Returns an array containing all the groups to which the current user administers.  Pass
 * $admin=FALSE to return all groups to which the current user belongs.
 * 
 * @param boolean $admin=TRUE
 * @return array 
 */
function og_affiliations_get_groups($admin = TRUE) {
  global $user;
  if (!$user->uid) {
    return FALSE;
  }
  $account = $user;
  if (!isset($account->og_groups)) {
    $account = user_load(array('uid' => $account->uid));
  }
  $gids = array();
  foreach ($account->og_groups as $nid => $attributes) {
    if (!$admin || $attributes['is_admin']) {
      $gids[] = $nid;
    }
  }
  return $gids;
}

/**
 * Returns an array containing all the users which administer a given group.  Pass
 * $admin=FALSE to return all users which belong to the given group.
 * 
 * @param integer $nid
 * @param boolean $admin=TRUE
 * @return array
 */
function og_affiliations_get_users($nid, $admin = TRUE) {
  $result = db_query(og_list_users_sql(1, ($admin ? 1 : 0), NULL), $nid);
  while ($row = db_fetch_object($result)) {
    $uids[] = $row->uid;
  }
  return $uids;
}

/**
 * Returns whether or not groups of type $atype can be affiliated to groups of
 * type $type.  Pass $in=FALSE to reverse this relationship (e.g., can $type
 * be affiliated to $atype).
 * 
 * @param string $type
 * @param string $atype
 * @param boolean $in=TRUE
 * @return boolean
 */
function og_affiliations_is_affiliate_type($type, $atype, $in = TRUE) {
  $variable = 'og_affiliations_'.($in ? $type.'_'.$atype : $atype.'_'.$type);
  return variable_get($variable, FALSE);
}

/**
 * Returns whether or not group $aid is affiliated to group $nid.  By default this
 * function only checks for active affiliations, but takes an optional status if
 * you want to check for a pending affiliation.
 * 
 * @param integer $nid
 * @param integer $aid
 * @param integer $status=OG_AFFILIATIONS_STATUS_ACTIVE
 * @return boolean
 */
function og_affiliations_is_affiliated($nid, $aid, $status = OG_AFFILIATIONS_STATUS_ACTIVE) {
  $count = db_result(db_query(OG_AFFILIATIONS_SQL_EXISTS_BY_STATUS, $nid, $aid, $status));
  return ($count > 0);
}

/**
 * Creates an affiliation from group $aid to group $nid.
 * 
 * @param integer $nid
 * @param integer $aid
 * @param array $args=array()
 */
function og_affiliations_save($nid, $aid, $args = array()) {
  $count = db_result(db_query(OG_AFFILIATIONS_SQL_EXISTS, $nid, $aid));
  $time = time();
  $affiliation = array(
    'nid' => $nid,
    'aid' => $aid,
    'created' => isset($args['created']) ? $args['created'] : $time,
    'changed' => $time,
  );
  $affiliation += $args;
  
  if ($count == 0) {
    drupal_write_record('og_affiliations', $affiliation);
//TODO
//    module_invoke_all('og', 'insert group affiliation', $nid, $aid, $args);
  }
  else {
    drupal_write_record('og_affiliations', $affiliation, array('nid', 'aid'));
//TODO
//    module_invoke_all('og', 'update group affiliation', $nid, $aid, $args);
  }
}
