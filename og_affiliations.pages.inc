<?php

/**
 * Page callback to administer affiliations (and requests) to the current group.
 * 
 * @param object $node
 * @param string $atype
 * @param string $direction
 * @param string $status
 * @return string
 */
function og_affiliations_administer($node, $atype, $direction, $status) {
  return views_embed_view('og_affiliations_'.$direction.'_'.$status, 'page_1', $node->nid, $atype);
}

/**
 * Simply redirects the request to the group admin page.  Useful so we could easily
 * add a menu item for group admins to the group node page.
 * 
 * @param object $node
 */
function og_affiliations_administer_redirect($node) {
  if (og_is_group_admin($node)) {
    drupal_goto('og/users/'.$node->nid);
  }
  drupal_goto('node/'.$node->nid);
}

/**
 * Page callback invoked when requesting affiliation.  Returns the confirmation
 * form or renders a warning/error message.
 * 
 * @param object $node
 * @param object $affiliate
 * @return mixed
 */
function og_affiliations_create($node, $affiliate) {
  if ($node->og_selective >= OG_INVITE_ONLY || $node->status == 0 || !og_is_group_type($node->type)) {
    drupal_set_message(t('Unable to create/request affiliation with %g at this time.', array('%g' => $node->title)));
    drupal_access_denied();
    exit();
  }
  
  if (og_affiliations_is_affiliated($node->nid, $affiliate->nid)) {
    drupal_set_message(t('%a is already affiliated with %g.', array('%a' => $affiliate->title, '%g' => $node->title)));
    drupal_goto('node/'.$affiliate->nid);
  }
  else {
    return drupal_get_form('og_affiliations_create_confirm', $node, $affiliate);
  }
}

/**
 * Form callback which returns the create affiliation confirmation form.
 * 
 * @param array $form_state
 * @param object $node
 * @param object $affiliate
 * return array
 */
function og_affiliations_create_confirm($form_state, $node, $affiliate) {
  $form['node'] = array('#type' => 'value', '#value' => $node);
  $form['affiliate'] = array('#type' => 'value', '#value' => $affiliate);
  return confirm_form(
    $form, 
    t('Are you sure you want %a to be affiliated with %g?', array('%a' => $affiliate->title, '%g' => $node->title)),
    'node/'.$affiliate->nid,
    ' ',
    t('Proceed'),
    t('Cancel')
  );
}

/**
 * Form callback which submits the create affiliation confirmation form.
 * 
 * @param array $form
 * @param array &$form_state
 */
function og_affiliations_create_confirm_submit($form, &$form_state) {
  $node = $form_state['values']['node'];
  $affiliate = $form_state['values']['affiliate'];
  $return = og_affiliations_affiliate($node, $affiliate);
  if (!empty($return['message'])) {
    drupal_set_message($return['message']);
  }
  //TODO redirect to user's original location (add via query string in original link)
  $form_state['redirect'] = 'node/'.$affiliate->nid;
}

/**
 * Page callback to manage affiliations between the current group and other groups
 * which the user administers.
 * 
 * @param object $node
 * @param string $atype
 * @return string
 */
function og_affiliations_manage($node, $atype) {
  return views_embed_view('og_affiliations_manage', 'page_1', $node->nid, $atype);
}

/**
 * Form callback which returns the remove affiliation confirmation form.
 * 
 * @param array $form_state
 * @param object $node
 * @param object $affiliate
 * return array
 */
function og_affiliations_remove_confirm($form_state, $node, $affiliate) {
  $form['node'] = array('#type' => 'value', '#value' => $node);
  $form['affiliate'] = array('#type' => 'value', '#value' => $affiliate);
  return confirm_form(
    $form,
    t('Are you sure you want to remove the affiliation between %a and %g?', array('%a' => $affiliate->title, '%g' => $node->title)),
    'node/'.$affiliate->nid,
    ' ',
    t('Proceed'),
    t('Cancel')
  );
}

/**
 * Form callback which submits the remove affiliation confirmation form.
 * 
 * @param array $form
 * @param array &$form_state
 */
function og_affiliations_remove_confirm_submit($form, &$form_state) {
  $node = $form_state['values']['node'];
  $affiliate = $form_state['values']['affiliate'];
  og_affiliations_delete($node->nid, $affiliate->nid);
  drupal_set_message(t('%a is no longer affiliated with %g.', array('%a' => $affiliate->title, '%g' => $node->title)));
  $form_state['redirect'] = 'node/'.$affiliate->nid;
}

/**
 * System settings form callback.
 * 
 * @return mixed
 */
function og_affiliations_settings() {
  $types = og_affiliations_get_group_types();
  if (count($types) > 0) {
    $_types = $types;
    
    $form['types'] = array(
      '#type' => 'fieldset',
      '#title' => t('Settings for the various group node types'),
    );
    
    // iterate over each type and render settings
    foreach ($types as $type) {
      $form['types'][$type] = array(
        '#type' => 'fieldset',
        '#title' => t('Group types which can be affiliated to %t groups', array('%t' => node_get_types('name', $type))),
      );
      foreach ($_types as $_type) {
        $form['types'][$type]['og_affiliations_'.$type.'_'.$_type] = array(
          '#type' => 'checkbox',
          '#title' => t(node_get_types('name', $_type)),
          '#default_value' => variable_get('og_affiliations_'.$type.'_'.$_type, FALSE),
        );
      }
    }
    return system_settings_form($form);
  }
  
  // if no group types are setup, forward user to OG admin section
  drupal_goto('admin/og/og');
}

/**
 * Page callback to view affiliations between the current group and other groups.
 * 
 * @param object $node
 * @param string $atype
 * @param string $direction
 * @return string
 */
function og_affiliations_view($node, $atype, $direction) {
  return views_embed_view('og_affiliations_'.$direction.'_active', 'page_2', $node->nid, $atype);
}
